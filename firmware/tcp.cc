#include "tcp.h"
#include <cassert>
#include <algorithm>

class hwprocessor {
public:
  struct ret_t {
    bool send;
    bool updateSize;
    u64  dataSize;
    ret_t(bool val) : send(val), updateSize(false), dataSize(0) {}
    ret_t(bool val, u64 size) : send(val), updateSize(true), dataSize(size) {}
  };

  ret_t process(const LBUS rx[4], u16 len, LBUS tx[4], const u48 MY_MAC, const u32 MY_IP, const u16 MY_PORT);
  ret_t nop(LBUS tx[4]);

private:
  const u16 IPV4 = (0x0800);
  const u16 ARP = (u16(0x0806));
  const u48 BC_MAC = 0xffffffffffff;

  ret_t process_arp(const LBUS rx[4], LBUS tx[4], const u48 MY_MAC, const u32 MY_IP);
  ret_t process_ipv4(const LBUS rx[4], u16 len, LBUS tx[4], const u32 MY_IP, const u16 MY_PORT);
  ret_t process_tcp(const LBUS rx[4], const u32 srcip, const u32 dstip, const u16 len, LBUS tx[4], u16& len_out, const u32 MY_IP, const u16 MY_PORT);

  u32 expseq_ = 0, expack_ = 0;
  enum TCPState { Listen = 0, SynReceived = 1, Established = 2, FinSent = 3 } tcpstate_ = Listen;
  u64 dataSize_ = 0;

  struct CheckSum {
    ap_uint<24> sum = 0;
    CheckSum(ap_uint<24> val = 0) : sum(val) {}
    inline CheckSum add(const u16& val) const { return CheckSum(sum + val); }
    inline CheckSum add32(const u32& val) const { return CheckSum(sum + u16(val(31, 16)) + u16(val(15, 0))); }
    inline CheckSum add64(const ap_uint<64>& val) const {
      return CheckSum(sum + u16(val(63, 48)) + u16(val(47, 32)) + u16(val(31, 16)) + u16(val(15, 0)));
    }
    u16 finish() const {
      ap_uint<18> sum1 = u16(sum(15, 0)) + u8(sum(23, 16));    // round one
      u16 sum2 = u16(sum1(15, 0)) + ap_uint<2>(sum1(17, 16));  // round two
      return ~sum2;
    }
  };
};

hwprocessor::ret_t hwprocessor::nop(LBUS tx[4]) {
#pragma HLS array_partition variable = tx complete
#pragma HLS inline
  for (int i = 0; i < 4; ++i) {
#pragma HLS unroll
    tx[i].enable = false;
  }
  return ret_t(false);
}

// https://en.wikipedia.org/wiki/Ethernet_frame
hwprocessor::ret_t hwprocessor::process_arp(const LBUS rx[4], LBUS tx[4], const u48 MY_MAC, const u32 MY_IP) {
#pragma HLS array_partition variable = rx complete
#pragma HLS array_partition variable = tx complete

  // https://en.wikipedia.org/wiki/Address_Resolution_Protocol
  u48 dmac, smac, sha, tha, padding;
  u16 etht, htype, ptype, hplen, oper;
  u32 spa, tpa;
  (dmac, smac, etht, htype) = rx[0].data;       // 6+6+2+2   = 16
  (ptype, hplen, oper, sha, spa) = rx[1].data;  // 2+2+2+6+4 = 16
  (tha, tpa, padding) = rx[2].data;             // 6+4+6     = 16

  assert(htype == 1 && ptype == IPV4 && sha == smac);
  if (oper == 1 && tpa == MY_IP) {
    // craft a reply
    tx[0].data(15, 0) = htype;
    tx[1].data = (ptype, hplen, u16(2), MY_MAC, MY_IP);
    tx[2].data = (sha, spa, padding);
    tx[3].data = 0;
    tx[0].mty = 0;
    tx[1].mty = 0;
    tx[2].mty = 0;
    tx[3].mty = 4;
    return ret_t(true);
  } else {
    return nop(tx);
  }
}

hwprocessor::ret_t hwprocessor::process_ipv4(const LBUS rx[4], u16 len, LBUS tx[4], const u32 MY_IP, const u16 MY_PORT) {
#pragma HLS array_partition variable = rx complete
#pragma HLS array_partition variable = tx complete
  // https://en.wikipedia.org/wiki/Ethernet_frame#Structure
  u48 eth_dmac, eth_smac;
  u16 eth_type;
  // https://en.wikipedia.org/wiki/IPv4#Packet_structure
  u4 ip_version, ip_ihl;
  u8 ip_dscpecn;
  u16 ip_len, ip_id, ip_frag;
  u8 ip_ttl, ip_proto;
  u16 ip_checksum;
  u32 ip_src, ip_dst;

  (eth_dmac, eth_smac, eth_type) = get_bytes<6 + 6 + 2>(rx[0].data);
  ap_uint<160> iphdr = (get_bytes<2, 6 + 6 + 2>(rx[0].data), rx[1].data, get_bytes<2>(rx[2].data));
  (ip_version, ip_ihl, ip_dscpecn, ip_len, ip_id, ip_frag, ip_ttl, ip_proto, ip_checksum, ip_src, ip_dst) = iphdr;

#ifndef __SYNTHESIS__
  int trailer = len.to_int() - ip_len.to_int();
  printf("HW: ip v%u, header length %u, packet length %u (eth pckt len %u, trailer %d), protocol %02x, src %08x, dst %08x\n",
         ip_version.to_uint(),
         ip_ihl.to_uint() * 4,
         ip_len.to_uint(),
         len.to_uint(),
         trailer,
         ip_proto.to_uint(),
         ip_src.to_uint(),
         ip_dst.to_uint());
  if (trailer < 0 || trailer >= 64) printf("HW: bogus packet len!\n");
#endif
  bool toolong = (ip_len > len);
  bool tooshort = (len >= ip_len + 64);
  u16 ip_len_out = u16(ip_ihl) << 2;
  ret_t ret(false);
  if (ip_proto == 6 && ip_ihl == 5 && ip_dst == MY_IP && !(toolong || tooshort)) {
    ret = process_tcp(rx, ip_src, ip_dst, ip_len - (u8(ip_ihl) << 2), tx, ip_len_out, MY_IP, MY_PORT);
  }

  // compute checksum
  ip_checksum = CheckSum()
                    .add((ip_version, ip_ihl, ip_dscpecn))
                    .add(ip_len_out)
                    .add(ip_id)
                    .add(ip_frag)
                    .add((u8(255), ip_proto))
                    .add32(ip_src)
                    .add32(ip_dst)
                    .finish();
  // assemble header
  iphdr = (ip_version, ip_ihl, ip_dscpecn, ip_len_out, ip_id, ip_frag, u8(255), ip_proto, ip_checksum, ip_dst, ip_src);

  // fill lbus
  set_bytes<2, 14>(tx[0].data, get_bytes<2, 0>(iphdr));
  set_bytes<16, 0>(tx[1].data, get_bytes<16, 2>(iphdr));
  set_bytes<2, 0>(tx[2].data, get_bytes<2, 18>(iphdr));
  return ret;
}

hwprocessor::ret_t hwprocessor::process_tcp(
    const LBUS rx[4], const u32 ip_src, const u32 ip_dst, const u16 len, LBUS tx[4], u16& len_out, const u32 MY_IP, const u16 MY_PORT) {
  // https://en.wikipedia.org/wiki/Transmission_Control_Protocol#TCP_segment_structure
  u16 sport, dport;
  u32 seqno, ackno;
  u4 hl;
  ap_uint<12> flags;
  u16 window, checksum, urg;
  ap_uint<160> tcphdr_min = (get_bytes<14, 2>(rx[2].data), get_bytes<6>(rx[3].data));
  (sport, dport, seqno, ackno, hl, flags, window, checksum, urg) = tcphdr_min;
  u8 hlen = u8(hl) << 2;
  u16 dtlen = len - hlen;
  bool syn = flags[1], ack = flags[4], fin = flags[0], rst = flags[2];
  bool port = (dport == MY_PORT);
  u32 expseq = expseq_;
  u32 expack = expack_;
  TCPState tcpstate = tcpstate_;
  u64 dataSize = dataSize_;
#ifndef __SYNTHESIS__
  printf("HW: TCP %u -> %u, length %u-%u=%u, seq %u (exp %u), ack %u (exp %u), flags %c%c%c%c, my state %d\n",
         sport.to_uint(),
         dport.to_uint(),
         len.to_uint(),
         hlen.to_uint(),
         dtlen.to_uint(),
         seqno.to_uint(),
         expseq.to_uint(),
         ackno.to_uint(),
         expack.to_uint(),
         (syn ? 'S' : '_'),
         (ack ? 'A' : '_'),
         (fin ? 'F' : '_'),
         (rst ? 'R' : '_'),
         int(tcpstate));
#endif
  ret_t ret(port);
  ap_uint<64> opts = 0;
  switch (tcpstate) {
    case Listen: {
      if (port && syn && !ack && !rst && !fin) {
        // crafy return
        expseq = seqno + 1;
        expack = 37;
        tcpstate = SynReceived;
        dataSize = dtlen;
        hl = 7;
        opts = (u8(2), u8(4), /*mss=*/u16(0x2300), u8(3), u8(3), /*window size=*/u8(7), u8(0));
        ack = true;
        ret = ret_t(true, dtlen);
      } else {
        ret = false;
      }
    } break;
    case SynReceived: {
      if (port && ack && !syn && !rst && !fin && seqno == expseq) {
        tcpstate = Established;
        expack++;
      } else {
        if (port && rst) 
            tcpstate = Listen;
      }
      ret = false;
    } break;
    case Established: {
      if (port && ack && !syn && !rst && seqno == expseq) {
        dataSize += dtlen;
        ret = ret_t(true, dataSize);
        // crafy return
        if (fin) {
          expseq = seqno + dtlen + 1;
          tcpstate = FinSent;
        } else {
          expseq = seqno + dtlen;
        }
        hl = 5;
      } else {
        if (port && rst)
          tcpstate = Listen;
        ret = false;
      }
    } break;
    case FinSent: {
      if (port && ack && !syn && !rst && seqno == expseq) {
        tcpstate = Listen;
      } else if (port && rst) {
        tcpstate = Listen;
      }
      ret = false;
    }
  }

  if (port) {
    tcpstate_ = tcpstate; expseq_ = expseq; expack_ = expack; dataSize_ = dataSize;
  }
  flags = 0;
  flags[0] = fin;
  flags[1] = syn;
  flags[2] = rst;
  flags[4] = ack;
  window = 0xFFFFFFFF;
  urg = 0;

  // https://en.wikipedia.org/wiki/Transmission_Control_Protocol#TCP_checksum_for_IPv4
  checksum = CheckSum(6)
                 .add32(ip_src)
                 .add32(ip_dst)
                 .add(u8(hl) << 2)
                 .add(sport)
                 .add(dport)
                 .add32(expseq)
                 .add32(expack)
                 .add((hl, flags))
                 .add(window)
                 .add(urg)
                 .add64(opts)
                 .finish();

  ap_uint<28 * 8> tcphdr_big = (dport, sport, expack, expseq, hl, flags, window, checksum, urg, opts);
  set_bytes<14, 2>(tx[2].data, get_bytes<14, 0>(tcphdr_big));
  set_bytes<14, 0>(tx[3].data, get_bytes<14, 14>(tcphdr_big));
  len_out = 20 + (u8(hl) << 2);
  return ret;
}
hwprocessor::ret_t hwprocessor::process(const LBUS rx[4], u16 len, LBUS tx[4], const u48 MY_MAC, const u32 MY_IP, const u16 MY_PORT) {
#pragma HLS array_partition variable = rx complete
#pragma HLS array_partition variable = tx complete
#pragma HLS pipeline II = 1
  ret_t reply(false);
  if (rx[0].enable && rx[0].start) {
    u48 dmac, smac;
    u16 etht;
    (dmac, smac, etht) = get_bytes<14>(rx[0].data);
    set_bytes<14>(tx[0].data, (smac, MY_MAC, etht));
#ifndef __SYNTHESIS__
    printf("HW: srcmac %012llx, dstmac %012llx, type %04x\n", smac.to_uint64(), dmac.to_uint64(), etht.to_uint());
#endif

    if (etht == ARP && dmac == BC_MAC) {
#ifndef __SYNTHESIS__
      printf("HW: this is an ARP to broadcast MAC\n");
#endif
      reply = process_arp(rx, tx, MY_MAC, MY_IP);
    } else if (dmac == MY_MAC && etht == IPV4) {
#ifndef __SYNTHESIS__
      printf("HW: this is an IPV4 to my MAC\n");
#endif
      reply = process_ipv4(rx, len, tx, MY_IP, MY_PORT);
    } else {
      reply = nop(tx);
    }
  } else {
    reply = nop(tx);
  }
  for (int i = 0; i < 4; ++i) {
    tx[i].enable = reply.send;
    tx[i].start = (i == 0);
    tx[i].end = (i == 3);
    tx[i].mty = 0;  //(i == 3 ? 4 : 0);
    tx[i].error = false;
  }
  return reply;
}

bool process_packet(const LBUS rx[4], LBUS tx[4], u64 & dataSize, const u48 mac, const u32 ip, const u16 port) {
#pragma HLS array_partition variable = rx complete
#pragma HLS array_partition variable = tx complete
#pragma HLS interface mode = ap_none port = rx
#pragma HLS interface mode = ap_none port = tx
#pragma HLS disaggregate variable = rx
#pragma HLS disaggregate variable = tx
#pragma HLS stable variable = mac
#pragma HLS stable variable = ip
#pragma HLS stable variable = port
#pragma HLS pipeline II = 1
#pragma HLS latency min = 1
  static hwprocessor proc;
  static LBUS rx_queue[4];
  static u16 len;
  static u64 dataSize_;
#pragma HLS array_partition variable = rx_queue complete
  bool go = false;
  u8 lengths[4];
  #pragma HLS array_partition variable = lengths complete
  for (int i = 0; i < 4; ++i) { 
    lengths[i] = (rx[i].enable ? u8(16 - rx[i].mty) : u8(0));
  }
  if (rx[0].enable && rx[0].start) {
    for (int i = 0; i < 4; ++i) {
      rx_queue[i] = rx[i];
    }
    len = lengths[0] + lengths[1] + lengths[2] + lengths[3];
  } else {
    len += lengths[0] + lengths[1] + lengths[2] + lengths[3];
  }
  for (int i = 0; i < 4; ++i) {
    if (rx[i].enable && rx[i].end && !rx[i].error) {
      go = true;
    }
  }
  LBUS tx_queue[4];
#pragma HLS array_partition variable = tx_queue complete
  hwprocessor::ret_t send = go ? proc.process(rx_queue, len, tx_queue, mac, ip, port) : hwprocessor::ret_t(false);
  for (int i = 0; i < 4; ++i) {
    tx[i].copy_or_null(go && send.send, tx_queue[i]);
  }
  if (send.updateSize) dataSize_ = send.dataSize;
  dataSize = dataSize_;
  return go && send.send;
}