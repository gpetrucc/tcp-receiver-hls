#ifndef tcp_hls_h
#define tcp_hls_h

#include <ap_int.h>

struct LBUS {
    ap_uint<128> data;
    bool enable, start, end, error;
    ap_uint<4> mty;
    void clear() {
        data = 0;
        enable = false;
        start = false;
        end = false;
        error = false;
        mty = 0;
    }
    void copy_or_null(bool copy, const LBUS & other) {
        data = copy ? other.data : ap_uint<128>(0);
        start = copy ? other.start : false;
        end = copy ? other.end : false;
        enable = copy ? other.enable : false;
        error = copy ? other.error : false;
        mty = copy ? other.mty : ap_uint<4>(0);        
    }
};

typedef ap_uint<4> u4;
typedef ap_uint<8> u8;
typedef ap_uint<16> u16;
typedef ap_uint<32> u32;
typedef ap_uint<48> u48;
typedef ap_uint<64> u64;

inline u16 byteswap(const u16 & val) {
    return (val(7,0),val(15,8));
}
inline u32 byteswap(const u32 & val) {
    return (val(7,0),val(15,8),val(23,16),val(31,24));
}
inline u48 byteswap(const u48 & val) {
    return (val(7,0),val(15,8),val(23,16),val(31,24),val(39,32),val(47,40));
}

bool process_packet(const LBUS rx[4], LBUS tx[4], u64 & dataSize, const u48 mac, const u32 ip, const u16 port) ;

template <int NBYTES, int OFFS=0, int N>
inline ap_uint<NBYTES*8> get_bytes(const ap_uint<N> & val) {
    ap_uint<NBYTES*8> ret;
    ret = val(N-1-8*OFFS,N-8*(NBYTES+OFFS));
    return ret;
}

template <int NBYTES, int OFFS=0, int N>
inline void set_bytes(ap_uint<N> & var, const ap_uint<NBYTES*8> & val) {
    var(N-1-8*OFFS,N-8*(NBYTES+OFFS)) = val;
}
#endif
