#include "firmware/tcp.h"
#include "ref/tcp_ref.h"
#include <cstdio>

void printLBUS(const LBUS x) {
  printf("%c%c%c%1x ", (x.enable ? 'V' : '_'), (x.start ? 'S' : '_'), (x.end ? 'E' : '_'), x.mty.to_uint());
  for (int b = 0; b < 16; ++b) {
    if (b < 16 - x.mty) {
      ap_uint<8> b8;
      b8 = x.data(127 - 8 * b, 120 - 8 * b);
      printf("%02x", b8.to_int());
    } else {
      printf("..");
    }
    if (b % 2 == 1)
      printf(" ");
  }
  printf("  ");
}
void printLBUS(const char *name, int i, const LBUS x[4]) {
  printf("  %s>> %04d: ", name, i);
  for (int s = 0; s < 4; ++s) {
    printLBUS(x[s]);
  }
  printf("\n");
}

void printPCKT(const char *name, const char *pckt, unsigned int len) {
  for (int i = 0; i < len; ++i) {
    if (i % 64 == 0) {
      printf("\n  %s>> %04d: ", name, i);
    }
    if (i % 16 == 0)
      printf("     ");
    printf("%02x", unsigned(pckt[i]) & 0xff);
    if (i % 2 == 1)
      printf(" ");
    if (i % 16 == 15)
      printf("  ");
    if (i >= 120)
      break;
  }
  printf("\n");
}

void pcktToLBUS(const char *pckt, unsigned int len, unsigned int row, LBUS rx[4]) {
  unsigned int ethlen = std::max(60u, len);  // +4 of CRC
  int i = 64 * row;
  for (int s = 0; s < 4; ++s) {
    if (i < ethlen) {
      rx[s].enable = 1;
      rx[s].start = (row == 0 && s == 0);
      rx[s].end = (ethlen - i <= 16);
      rx[s].mty = std::max<int>(i + 16 - ethlen, 0);
      rx[s].error = 0;
      rx[s].data = 0;
      for (int b = 0; b < 16; ++b, ++i) {
        rx[s].data(127 - 8 * b, 120 - 8 * b) = ap_uint<8>(i < len ? pckt[i] : '\0');
      }
    } else {
      rx[s].enable = 0;
    }
  }
}
int main(int argc, char **argv) {
  uint64_t myMAC = 0xb8cef6a4653flu;
  uint32_t myIP = 0x0a000003u;
  uint16_t myPORT = 7777;
  uint64_t dataSize_ref = 0;
  u64 dataSize = 0;
  processor proc(myMAC, myIP, myPORT, false, nullptr);
  LBUS rx[4], tx[4], txout[4], txref[4];

  for (int i = 1, ni = atoi(argv[1]); i <= ni; ++i) {
    char namebuff[25];
    snprintf(namebuff, 24u, "in.%d", i);
    FILE *fin = fopen(namebuff, "rb");
    if (!fin) {
      printf("ERROR, can't read %s\n", namebuff);
      return 1;
    }
    char buff_in[10240], buff_out[10240];
    int length_in;
    unsigned int length_out;
    fread(&length_in, sizeof(int), 1, fin);
    fread(buff_in, length_in, 1, fin);
    fclose(fin);
    printf("Injecting packet %d of length %d:", i, length_in);
    printPCKT("I ", buff_in, length_in);
    bool replied = false, replied_ref = false;
    for (int f = 0; f * 64 < length_in; ++f) {
      pcktToLBUS(buff_in, length_in, f, rx);
      printLBUS("RX", f, rx);
      if (process_packet(rx, tx, dataSize, myMAC, myIP, myPORT)) {
        printLBUS("TX", f, tx);
        for (int i = 0; i < 4; ++i)
          txout[i] = tx[i];
        replied = true;
        printf("Hardware  data size now %lu\n", dataSize.to_uint64());
      }
    }

    if (proc.process(reinterpret_cast<const uint8_t *>(buff_in),
                     length_in,
                     reinterpret_cast<uint8_t *>(buff_out),
                     length_out,
                     dataSize_ref)) {
      printf("Reference reply of length %u:", length_out);
      printPCKT("R ", buff_out, length_out);
      printf("Reference data size now %lu\n", dataSize_ref);
      replied_ref = true;
    }

    if (replied != replied_ref) {
      printf("ERROR: hls replied %d, ref replied %d\n", int(replied), int(replied_ref));
      return 2;
    } else if (replied_ref && dataSize_ref > 0 && dataSize.to_uint64() != dataSize_ref) {
      printf("ERROR: hls data size %lu, ref %lu\n", dataSize.to_uint64(), dataSize_ref);
    } else if (replied) {
      pcktToLBUS(buff_out, length_out, 0, txref);
      printLBUS("HLS", 0, txout);
      printLBUS("REF", 0, txref);
      bool ok = true;
      for (int i = 0; i < 4; ++i) {
        if (txout[i].enable != txref[i].enable) {
          printf("Mismatch in enable bit for segment %d", i);
          ok = false;
        }
        if (txout[i].start != txref[i].start) {
          printf("Mismatch in start bit for segment %d", i);
          ok = false;
        }
        if (txout[i].end != txref[i].end) {
          printf("Mismatch in end bit for segment %d", i);
          ok = false;
        }
      }
      for (unsigned int b = 0; b < length_out; ++b) {
        unsigned out = u8(txout[b / 16].data(8 * (16 - (b % 16)) - 1, 8 * (16 - (b % 16)) - 8)).to_uint();
        unsigned ref = u8(txref[b / 16].data(8 * (16 - (b % 16)) - 1, 8 * (16 - (b % 16)) - 8)).to_uint();
        if (out != ref) {
          printf("Mismatch at byte %u: hls %02x, ref %02x\n", b, out, ref);
          ok = false;
          break;
        }
      }
      if (!ok)
        return 3;
    }
  }
}