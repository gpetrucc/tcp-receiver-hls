#include <fcntl.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <linux/if.h>
#include <linux/if_tun.h>
#include "tcp_ref.h"
#include <algorithm>
#include <cstdio>
#include <cstring>

int tun_alloc(char *dev, int flags) {
  struct ifreq ifr;
  int fd, err;
  const char *clonedev = "/dev/net/tun";

  /* Arguments taken by the function:
   *
   * char *dev: the name of an interface (or '\0'). MUST have enough
   *   space to hold the interface name if '\0' is passed
   * int flags: interface flags (eg, IFF_TUN etc.)
   */

  /* open the clone device */
  if ((fd = open(clonedev, O_RDWR)) < 0) {
    return fd;
  }

  /* preparation of the struct ifr, of type "struct ifreq" */
  std::memset(&ifr, 0, sizeof(ifr));

  ifr.ifr_flags = flags; /* IFF_TUN or IFF_TAP, plus maybe IFF_NO_PI */

  if (*dev) {
    /* if a device name was specified, put it in the structure; otherwise,
      * the kernel will try to allocate the "next" device of the
      * specified type */
    std::strncpy(ifr.ifr_name, dev, IFNAMSIZ);
  }

  /* try to create the device */
  if ((err = ioctl(fd, TUNSETIFF, (void *)&ifr)) < 0) {
    close(fd);
    return err;
  }

  /* if the operation was successful, write back the name of the
   * interface to the variable "dev", so the caller can know
   * it. Note that the caller MUST reserve space in *dev (see calling
   * code below) */
  std::strcpy(dev, ifr.ifr_name);

  /* this is the special file descriptor that the caller will use to talk
   * with the virtual interface */
  return fd;
}

int main(int argc, char *argv[]) {
  char devname[100];
  char buff_in[10000];
  char buff_out[10000];

  char pcktdump_fname[255];

  if (argc != 3) {
    printf("Usage: %s packets_out_dir  out_file\n", argv[0]);
  }
  FILE *fout = fopen(argv[2], "wb");
  if (argc > 1) {
    printf("Will write packet output to %s\n", argv[1]);
  } else {
    return 1;
  }
  int fd = tun_alloc(devname, IFF_TAP | IFF_NO_PI);
  if (fd < 0) {
    printf("Error opening %s\n", devname);
    return 2;
  } else {
    printf("Opened %s on fd %d\n", devname, fd);
  }
  ioctl(fd, TUNSETNOCSUM, 1);

  uint64_t myMAC = 0xb8cef6a4653flu;
  uint32_t myIP = 0x0a000003u;
  uint16_t myPORT = 7777;
  uint64_t dataSize = 0;
  processor proc(myMAC, myIP, myPORT, false, fout);
  unsigned int npkt_in = 0, npkt_out = 0, npckt_in_all = 0;
  while (1) {
    int l_in = read(fd, buff_in, sizeof(buff_in));
    if (l_in == -1)
      continue;

    unsigned int l_out = sizeof(buff_out);
    if (l_in) {
      uint16_t type = ((unsigned(buff_in[12]) & 0xFF) << 8) | (unsigned(buff_in[13]) & 0xFF);
      printf("Received packet of length %d, type %04x\n", l_in, type);
      if (type != 0x86dd) {
        npckt_in_all++;
        //if (npckt_in_all == 7 || npckt_in_all == 17) {
        //  printf(" ---> Simulating loss of packet !\n");
        //  continue;
        //}
        npkt_in++;
        sprintf(pcktdump_fname, "%s/in.%u", argv[1], npkt_in);
        FILE *dump = fopen(pcktdump_fname, "wb");
        fwrite(&l_in, sizeof(int), 1, dump);
        fwrite(buff_in, l_in, 1, dump);
        fclose(dump);
      }
    }
    if (proc.process(reinterpret_cast<const uint8_t *>(buff_in), l_in, reinterpret_cast<uint8_t *>(buff_out), l_out, dataSize)) {
      npkt_out++;
      printf("Sending packet %u of length %d\n", npkt_out, l_out);
      sprintf(pcktdump_fname, "%s/out.%u", argv[1], npkt_out);
      FILE *dump = fopen(pcktdump_fname, "wb");
      fwrite(&l_out, sizeof(int), 1, dump);
      fwrite(buff_out, l_out, 1, dump);
      fclose(dump);
      write(fd, buff_out, l_out);
    }
  }

  proc.closeFile();
}
