#include "tcp_ref.h"
#include <cassert>
#include <cstring>

template <typename T, unsigned int N = sizeof(T)>
inline T get_(const uint8_t *pkt, unsigned int offs) {
  T ret(0);
  for (unsigned int b = 0; b < N; ++b) {
    ret = (ret << 8) | T(pkt[offs + b]);
  }
  return ret;
}
template <typename T, unsigned int N = sizeof(T)>
inline void set_(uint8_t *pkt, unsigned int offs, T val) {
  for (unsigned int b = 0; b < N; ++b) {
    pkt[offs + (N - b - 1)] = (val & 0xFF);
    val >>= 8;
  }
}
uint16_t inline get16(const uint8_t *pkt, unsigned int offs) { return get_<uint16_t>(pkt, offs); }
void inline set16(uint8_t *pkt, unsigned int offs, uint16_t val) { set_(pkt, offs, val); }

uint32_t inline get32(const uint8_t *pkt, unsigned int offs) { return get_<uint32_t>(pkt, offs); }
void inline set32(uint8_t *pkt, unsigned int offs, uint32_t val) { set_(pkt, offs, val); }

uint64_t inline get48(const uint8_t *pkt, unsigned int offs) { return get_<uint64_t, 6>(pkt, offs); }
void inline set48(uint8_t *pkt, unsigned int offs, uint64_t val) { set_<uint64_t, 6>(pkt, offs, val); }

uint64_t inline get64(const uint8_t *pkt, unsigned int offs) { return get_<uint64_t>(pkt, offs); }
void inline set64(uint8_t *pkt, unsigned int offs, uint64_t val) { set_(pkt, offs, val); }

uint32_t processor::sum16(const uint8_t *pkt, unsigned int len) {
  uint32_t sum = 0;
  assert(len % 2 == 0);
  for (unsigned int i = 0; i < len; i += 2) {
    sum += get16(pkt, i);
  }
  return sum;
}
uint16_t processor::finishChecksum(uint32_t sum) {
  sum = (sum & 0xFFFF) + (sum >> 16);  // round one
  sum = (sum & 0xFFFF) + (sum >> 16);  // round two
  uint16_t ret = ~(uint16_t(sum & 0xFFFF));
  return ret;
}

bool processor::process(const uint8_t *pkt_in, unsigned int len_in, uint8_t *pkt_out, unsigned int &len_out, uint64_t & tcp_data_out_size) {
  uint64_t dmac = get48(pkt_in, 0);
  uint64_t smac = get48(pkt_in, 6);
  uint16_t type = get16(pkt_in, 12);
  printf(">> ETH header: src %012llx dst %012llx type %04x\n", smac, dmac, type);
  bool ret = false;
  if (type == ETH_TYPE_ARP && dmac == ETH_BROADCAST_MAC) {
    ret = process_arp(pkt_in + ETH_HDR_LEN, len_in - ETH_HDR_LEN, pkt_out + ETH_HDR_LEN, len_out);
  } else if (type == ETH_TYPE_IPV4 && dmac == mymac_) {
    ret = process_ipv4(pkt_in + ETH_HDR_LEN, len_in - ETH_HDR_LEN, pkt_out + ETH_HDR_LEN, len_out, tcp_data_out_size);
  }
  if (ret) {
    set48(pkt_out, 0, smac);
    set48(pkt_out, 6, mymac_);
    set16(pkt_out, 12, type);
    len_out += ETH_HDR_LEN;
  }
  return ret;
}

bool processor::process_arp(const uint8_t *pkt_in, unsigned int len_in, uint8_t *pkt_out, unsigned int &len_out) {
  uint16_t htype = get16(pkt_in, 0);
  uint16_t ptype = get16(pkt_in, 2);
  uint16_t hplen = get16(pkt_in, 4);
  uint16_t oper = get16(pkt_in, 6);
  uint64_t sha = get48(pkt_in, 8);
  uint32_t spa = get32(pkt_in, 14);
  uint64_t tha = get48(pkt_in, 18);
  uint32_t tpa = get32(pkt_in, 24);
  printf(">> ARP %u: sha %012llx, spa %08x, tha %012llx, tpa %08x\n", oper, sha, spa, tha, tpa);
  if (oper == 1 && tpa == myip_) {
    printf(">> Received ARP request from %012llx (ip %08x) for ip %08x\n", sha, spa, tpa);
    std::memcpy(pkt_out, pkt_in, ARP_LEN);
    set16(pkt_out, 6, 2);
    set48(pkt_out, 8, mymac_);
    set32(pkt_out, 14, myip_);
    set48(pkt_out, 18, sha);
    set32(pkt_out, 24, spa);
    len_out = ARP_LEN;
    return true;
  } else {
    return false;
  }
}

bool processor::process_ipv4(const uint8_t *pkt_in, unsigned int len_in, uint8_t *pkt_out, unsigned int &len_out, uint64_t & tcp_data_out_size) {
  uint8_t ihl = pkt_in[0] & 0xF;
  uint16_t len = get16(pkt_in, 2);
  uint8_t proto = pkt_in[9];
  uint16_t chksum = get16(pkt_in, 10);
  uint32_t saddr = get32(pkt_in, 12);
  uint32_t daddr = get32(pkt_in, 16);
  unsigned int hlen = ihl * 4;
  if (daddr == myip_ && proto == IP_PROTO_TCP) {
    printf(">> Received TCP request ip %08x for ip %08x, header %u, packet %u (check %u)\n",
           saddr,
           daddr,
           unsigned(ihl),
           unsigned(len),
           len_in);
    int trailer = int(len_in) - int(len);
    bool badlen = (trailer < 0 || trailer >= 64);
    if (badlen) {
      printf(">> Error, truncated packet (actual ip len %u vs reported %u, trailer %d), discarding it.\n",
             len_in,
             unsigned(len),
             trailer);
      if (!dupack_)
        return false;
    }
    if (process_tcp(
            pkt_in + hlen, len_in - hlen, saddr, daddr, len - hlen, badlen, pkt_out + IPV4_HDR_MIN_LEN, len_out, tcp_data_out_size)) {
      hlen = IPV4_HDR_MIN_LEN;
      std::memcpy(pkt_out, pkt_in, hlen);
      pkt_out[0] = (pkt_in[0] & 0xF0) | ((hlen / 4) & 0xF);
      set32(pkt_out, 12, daddr);
      set32(pkt_out, 16, saddr);
      set16(pkt_out, 2, len_out + hlen);
      pkt_out[8] = 255;
      set16(pkt_out, 10, 0);
      uint32_t sum = sum16(pkt_out, IPV4_HDR_MIN_LEN);
      set16(pkt_out, 10, finishChecksum(sum));
      len_out += IPV4_HDR_MIN_LEN;
      return true;
    }
  }
  return false;
}

bool processor::process_tcp(const uint8_t *pkt_in,
                            unsigned int len_in,
                            uint32_t srcip,
                            uint32_t dstip,
                            unsigned int len,
                            bool badpckt,
                            uint8_t *pkt_out,
                            unsigned int &len_out, 
                            uint64_t & tcp_data_out_size) {
  uint16_t sport = get16(pkt_in, 0);
  uint16_t dport = get16(pkt_in, 2);
  uint32_t sqno = get32(pkt_in, 4);
  uint32_t acno = get32(pkt_in, 8);
  uint8_t doffs = (pkt_in[12] >> 4) & 0xF;
  uint8_t flags = pkt_in[13];
  unsigned int hlen = doffs * 4;
  bool syn = flags & 0x2, rst = flags & 0x4, ack = flags & 0x10, fin = flags & 0x1;
  for (Listener & listener : listeners_) {
    if (dport == listener.myport_) {
      printf(
          ">> Received TCP from %08x:%u to %08x:%u, tcp length %u-%u=%u, seq %u, ack %u, flags %c%c%c%c, %s, my state "
          "is %d\n",
          srcip,
          sport,
          dstip,
          dport,
          len,
          hlen,
          len - hlen,
          sqno,
          acno,
          (syn ? 'S' : '_'),
          (ack ? 'A' : '_'),
          (fin ? 'F' : '_'),
          (rst ? 'R' : '_'),
          (badpckt ? "BAD" : "good"),
          int(listener.state_));
      if (listener.state_ == State::Listen && syn) {
        assert(!ack && !rst);
        printf(">> Received TCP connection request on port %u header len %u (my state %d)\n",
               unsigned(dport),
               unsigned(doffs));
        set16(pkt_out, 0, dport);
        set16(pkt_out, 2, sport);
        set32(pkt_out, 4, TCP_INITIAL_SYN);  // random initial syn number
        set32(pkt_out, 8, sqno + 1);
        pkt_out[12] = (TCP_HDR_SYN_LEN / 4) << 4;
        pkt_out[13] = 0x2 | 0x10;  // SYN, ACK
        set16(pkt_out, 14, 0xFFFF);
        set16(pkt_out, 16, 0);           // checksum
        set16(pkt_out, 18, 0);           // urgent
        pkt_out[20] = 2;                 // MSS: kind
        pkt_out[21] = 4;                 // MSS: length
        set16(pkt_out, 22, TCP_MY_MSS);  // MSS: value
        pkt_out[24] = 3;                 // Window scale: kind
        pkt_out[25] = 3;                 // Window scale: length
        pkt_out[26] = 7;                 // Window scale: length
        pkt_out[27] = 0;                 // End options
        len_out = TCP_HDR_SYN_LEN;
        uint32_t sum = 6;  // protocol number
        sum += (srcip & 0xFFFF) + (srcip >> 16);
        sum += (dstip & 0xFFFF) + (dstip >> 16);
        sum += TCP_HDR_SYN_LEN;
        sum += sum16(pkt_out, len_out);
        set16(pkt_out, 16, finishChecksum(sum));
        listener.state_ = State::SynReceived;
        listener.expectedSeqNo_ = sqno + 1;
        listener.receivedDataSize_ = len - hlen;
        tcp_data_out_size = listener.receivedDataSize_;
        printf(">> Now in state %d\n", int(listener.state_));
        return true;
      } else if (listener.state_ == State::SynReceived && ack) {
        printf(">> TCP connection data on port %u header len %u: 3-way handshake completed\n"),
            listener.state_ = State::Established;
        printf(">> Now in state %d\n", int(listener.state_));
        return false;  // don't ack this ack
      } else if ((listener.state_ == State::Established) && ack) {
        printf(">> Received TCP connection data on port %u header len %u, data len %u, seqno %u (exp %u), fin %d\n",
               unsigned(dport),
               unsigned(doffs),
               len - hlen,
               sqno,
               listener.expectedSeqNo_,
               int(fin));
        if (listener.expectedSeqNo_ != sqno) {
          printf(">> Bad sequence number, ignoring.\n");
          return false;
        }
        set16(pkt_out, 0, dport);
        set16(pkt_out, 2, sport);
        set32(pkt_out, 4, TCP_INITIAL_SYN + 1);
        set32(pkt_out, 8, sqno + (len - hlen) + (fin ? 1 : 0));
        pkt_out[12] = (TCP_HDR_DATA_LEN / 4) << 4;
        pkt_out[13] = 0x10 + (fin ? 0x1 : 0);  // ACK, + FIN if set
        set16(pkt_out, 14, 0xFFFF);
        set16(pkt_out, 16, 0);  // checksum
        set16(pkt_out, 18, 0);  // urgent
        len_out = TCP_HDR_DATA_LEN;
        uint32_t sum = 6;  // protocol number
        sum += (srcip & 0xFFFF) + (srcip >> 16);
        sum += (dstip & 0xFFFF) + (dstip >> 16);
        sum += TCP_HDR_DATA_LEN;
        sum += sum16(pkt_out, len_out);
        set16(pkt_out, 16, finishChecksum(sum));
        listener.receivedDataSize_ += len - hlen;
        tcp_data_out_size = listener.receivedDataSize_;
        if (fout_ != nullptr && len > hlen) {
          fwrite(pkt_in + hlen, len - hlen, 1, fout_);
        }
        if (fin) {
          listener.state_ = State::FinSent;
          listener.expectedSeqNo_ = sqno + (len - hlen) + 1;
          closeFile();
        } else {
          listener.expectedSeqNo_ = sqno + (len - hlen);
        }
        printf(">> Now in state %d\n", int(listener.state_));
        return true;
      } else if (listener.state_ == State::FinSent && ack) {
        printf(">> TCP connection data on port %u: closing handshake completed\n"), listener.state_ = State::Listen;
        return false;  // don't ack this ack
      } else if (rst) {
        printf(">> TCP connection data on port %u: connection reset\n"), closeFile();
        listener.state_ = State::Listen;
        // no ack?
        return false;
      }
    } else {
      printf(">> Listener on port %u will ignore TCP from %08x:%u to %08x:%u\n", listener.myport_,
             srcip, sport, dstip, dport);
    }
  }
  return false;
}

void processor::closeFile() {
  if (fout_ != nullptr) {
    printf(">> Closed output file.\n");
    fclose(fout_);
    fout_ = nullptr;
  }
}